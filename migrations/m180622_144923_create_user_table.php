<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user`.
 */
class m180622_144923_create_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('user', [
            'id' => $this->primaryKey(),
            'name'=> $this->string(),
            'username' => $this->string()->unique(),
            'password' => $this->string(),
           
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('user');
    }
}

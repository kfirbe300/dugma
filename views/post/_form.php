<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Category;
use yii\helpers\ArrayHelper;
use app\models\Post;
use app\models\Status;
use app\models\User;


/* @var $this yii\web\View */
/* @var $model app\models\Post */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="post-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'body')->textarea(['rows' => 6]) ?>

     <?= $form->field($model, 'category_id')->dropDownList(
             ArrayHelper::map(Category::find()->asArray()->all(), 'id', 'category_name'))
    ?> 

    <?= $form->field($model, 'status')->dropDownList(
             ArrayHelper::map(Status::find()->asArray()->all(), 'id', 'status_name'))
    ?>

   <?= $form->field($model, 'author')->dropDownList(
             ArrayHelper::map(User::find()->asArray()->all(), 'id', 'username'))
    ?>
    
    <?= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'updated_at')->textInput() ?>

    <?= $form->field($model, 'created_by')->textInput() ?>

    <?= $form->field($model, 'updated_by')->textInput() ?>

   

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
